use crate::gl::gl_enum::{GL_STATIC_DRAW, GL_FLOAT, GL_TRIANGLES, GL_UNSIGNED_INT, GL_FALSE, GL_PATCHES};

use sp_scene::sp_mesh::mesh::Mesh;
use crate::gl::buffers::{VAO, VBO};
use crate::gl::Gl;
use crate::gl::shader::shader_program::ShaderProgram;

use super::camera::Camera;

#[derive(Clone)]
pub struct MeshRenderer {
    pub vao: VAO,
    pub vbo: VBO,
    indices: Vec<u32>,
}

impl MeshRenderer {
    pub fn new(gl:&Gl, mesh: &Mesh) -> Self {
        let vao = VAO::new(gl);
        vao.bind(gl);

        let vbo = VBO::new(gl);
        vbo.bind(&gl);
        vbo.set_data(gl, mesh.get_vertices().as_slice(), GL_STATIC_DRAW);

        // position attribute
        gl.enable_vertex_attrib_array(0);
        gl.vertex_attrib_pointer::<f32>(
            0,
            3,
            GL_FLOAT,
            GL_FALSE,
            8,
            0,
        );

        // normal attribute
        gl.enable_vertex_attrib_array(1);
        gl.vertex_attrib_pointer::<f32>(
            1,
            3,
            GL_FLOAT,
            GL_FALSE,
            8,
            3,
        );

        // texture coordinates attribute
        gl.enable_vertex_attrib_array(2);
        gl.vertex_attrib_pointer::<f32>(
            2,
            2,
            GL_FLOAT,
            GL_FALSE,
            8,
            6,
        );

        let mut indices = Vec::<u32>::with_capacity(mesh.get_nb_faces() * 3);
        for f in mesh.get_faces() {
            indices.push(f.v1 as u32);
            indices.push(f.v2 as u32);
            indices.push(f.v3 as u32);
        }

        Self {
            vao,
            vbo,
            indices,
        }
    }

    pub fn draw (&self, gl: &Gl, _camera: &Camera, shader: &ShaderProgram) {
        self.vao.bind(gl);

        gl.enable_vertex_attrib_array(0);
        gl.enable_vertex_attrib_array(1);
        gl.enable_vertex_attrib_array(2);

        gl.draw_elements(if shader.has_tesselation() {GL_PATCHES} else {GL_TRIANGLES},
                         self.indices.len() as i32,
                         GL_UNSIGNED_INT,
                         self.indices.as_slice(),
        );

        gl.disable_vertex_attrib_array(2);
        gl.disable_vertex_attrib_array(1);
        gl.disable_vertex_attrib_array(0);

        self.vao.unbind(gl);
    }

    pub fn clean (&self, gl: &Gl) {
        gl.disable_vertex_array_attrib(self.vao.id, 0);
        gl.disable_vertex_array_attrib(self.vao.id, 1);
        gl.disable_vertex_array_attrib(self.vao.id, 2);

        self.vao.clean(gl);
        self.vbo.delete(gl);
    }
}