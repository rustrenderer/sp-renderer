use crate::graphical_objects::mesh_renderer::MeshRenderer;
use sp_scene::Scene;
use crate::gl::Gl;
use crate::graphical_objects::camera::Camera;
use crate::gl::shader::shader_program::ShaderProgram;
use sp_scene::sp_mesh::sp_math::{matrix::Matrix4f, vector::Vector2f};

use crate::gl::gl_enum::GL_VIEWPORT;
use sp_scene::material::Material;

#[derive(Clone)]
pub struct SceneRenderer {
    meshes: Vec<MeshRenderer>,
    transforms: Vec<Matrix4f>,
    materials: Vec<Option<Material>>
}

impl SceneRenderer {
    pub fn new (gl: &Gl, scene: &Scene) -> Self {
        Self {
            meshes: scene.get_meshes()
                         .iter()
                         .map(|m| {MeshRenderer::new(gl, *m)})
                         .collect(),
            transforms: scene.get_meshes()
                             .iter()
                             .enumerate()
                             .map(|(id, _)| { scene.get_mesh_transform (id) })
                             .collect(),
            materials: scene.get_meshes()
                            .iter()
                            .enumerate()
                            .map(|(id, _)| { scene.get_material (id).cloned() })
                            .collect(),
        }
    }

    pub fn draw (&self, gl: &Gl, camera: &Camera, shader: &ShaderProgram) {
        let mut viewport_data: [i32; 4] = [0,0,0,0];
        gl.get_integer_v(GL_VIEWPORT, &mut viewport_data);

        let win_size = Vector2f::new(viewport_data[2] as f32, viewport_data[3] as f32);

        let view = camera.look_at();
        let projection = Camera::perspective(60.0, win_size.x()/win_size.y(), 0.1, 1000.0);

        let view_loc = shader.get_uniform_location(&gl, String::from("view"));
        view_loc.set_mat_4f(&gl, true, &view);
        let projection_loc = shader.get_uniform_location(&gl, String::from("projection"));
        projection_loc.set_mat_4f(&gl, true, &projection);

        for ((mesh, transform), mat) in
            self.meshes.iter()
                .zip(self.transforms.iter())
                .zip(self.materials.iter())
        {
            let default_mat = Material::new("temp".to_string());
            let m = match mat {
                None => {&default_mat},
                Some(_mat) => {_mat},
            };

            let specular_coefficient_loc = shader.get_uniform_location(&gl, String::from("specular_coefficient"));
            specular_coefficient_loc.set_1f(&gl, m.specular_coefficient);

            let alpha_loc = shader.get_uniform_location(&gl, String::from("alpha"));
            alpha_loc.set_1f(&gl, m.alpha);

            let color_ambient_loc = shader.get_uniform_location(&gl, String::from("color_ambient"));
            color_ambient_loc.set_3fv(&gl, &m.color_ambient);

            let color_diffuse_loc = shader.get_uniform_location(&gl, String::from("color_diffuse"));
            color_diffuse_loc.set_3fv(&gl, &m.color_diffuse);

            let color_specular_loc = shader.get_uniform_location(&gl, String::from("color_specular"));
            color_specular_loc.set_3fv(&gl, &m.color_specular);

            let model_loc = shader.get_uniform_location(&gl, String::from("model"));
            model_loc.set_mat_4f(&gl, true, transform);

            let mvp = projection.clone() * view.clone() * transform.clone();
            let mvp_loc = shader.get_uniform_location(&gl, String::from("mvp"));
            mvp_loc.set_mat_4f(&gl, true, &mvp);

            mesh.draw(gl, camera, shader);
        }
    }

    pub fn clean (&self, gl: &Gl) {
        for m in self.meshes.iter() {
            m.clean(gl);
        }
    }

    pub fn get_nb_meshes (&self) -> usize {
        self.meshes.len()
    }
}