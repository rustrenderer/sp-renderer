use sp_scene::sp_mesh::sp_math::vector::Vector3f;
use sp_scene::sp_mesh::sp_math::matrix::Matrix4f;

#[derive(Clone)]
pub struct Camera {
    position: Vector3f,
    direction: Vector3f,
    up: Vector3f,
    right: Vector3f,
}

impl Camera {
    pub fn new (position: Vector3f, target: Vector3f) -> Self {
        let direction = (position - target).normalized();

        let up = Vector3f::new(0.0, 1.0, 0.0);
        let right = up.cross(direction);
        let up = direction.cross(right);

        Self {
            position,
            direction,
            up,
            right,
        }
    }

    pub fn look_at(&self) -> Matrix4f {
        Matrix4f::base(&self.right, &self.up, &self.direction) * Matrix4f::translation(&(self.position * -1.0f32))
    }

    pub fn position(&self) -> &Vector3f {
        &self.position
    }

    pub fn perspective (fov: f32, aspect_ratio: f32, near: f32, far: f32) -> Matrix4f {
        let scale = (fov.to_radians() * 0.5).tan() * near;
        let top = scale;
        let bottom = -top;
        let right = aspect_ratio * scale;
        let left = -right;

        Matrix4f::perspective(left, right, top, bottom, far, near)
    }

    pub fn orthographic (width: f32, height:f32, near: f32, far: f32) -> Matrix4f {
        let top = height / 2.0;
        let bottom = -top;
        let right = width / 2.0;
        let left = -right;

        Matrix4f::orthographic(left, right, top, bottom, far, near)
    }
}