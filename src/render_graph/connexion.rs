use sp_scene::sp_mesh::sp_math::vector::{Vector2f, Vector3f};
use sp_scene::sp_mesh::sp_math::matrix::Matrix4f;
use crate::graphical_objects::scene_renderer::SceneRenderer;
use crate::gl::shader::shader_program::ShaderProgram;
use crate::gl::texture::Texture;
use crate::graphical_objects::camera::Camera;
use crate::graphical_objects::render_target::RenderTarget;
use sp_scene::material::Material;

#[derive(Clone)]
pub enum ConnexionData {
    Int(i32),
    Unsigned(u32),
    Float(f32),
    Boolean(bool),
    String(String),
    Vector2f(Vector2f),
    Vector3f(Vector3f),
    Matrix4f(Matrix4f),
    Material(Material),
    Scene(SceneRenderer),
    Shader(ShaderProgram),
    Texture(Texture),
    Camera(Camera),
    RenderTarget(RenderTarget)
}

pub struct Connexion {
    pub from_node: usize,
    pub from_name: String,
    pub to_node: usize,
    pub to_name: String,
}