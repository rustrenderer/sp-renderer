use std::collections::{HashMap, BinaryHeap};
use crate::render_graph::connexion::{Connexion, ConnexionData};
use crate::gl::Gl;
use std::cmp::Ordering;
use crate::nodes::viewer_node::ViewerNode;

pub mod connexion;

pub struct NodeInterface {
    pub name: String,
    pub data: Option<ConnexionData>,
}

pub trait RenderNode {
    fn evaluate(&mut self, gl: &Gl);
    fn update(&mut self, gl: &Gl, delta_time: f32);

    fn get_input_names(&self) -> Vec<String>;
    fn set_input(&mut self, name: String, value: Option<ConnexionData>);

    fn get_output_names(&self) -> Vec<String>;
    fn get_output(&self, name: String) -> Option<&NodeInterface>;
}

struct Evaluation {
    pub evaluation_priority : usize,
    pub evaluation_target : usize,
}

impl PartialOrd for Evaluation {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.evaluation_priority.cmp(&other.evaluation_priority))
    }


}

impl PartialEq for Evaluation {
    fn eq(&self, other: &Self) -> bool {
        self.evaluation_priority == other.evaluation_priority
    }
}

impl Eq for Evaluation {}

impl Ord for Evaluation {
    fn cmp(&self, other: &Self) -> Ordering {
        self.evaluation_priority.cmp(&other.evaluation_priority)
    }
}

pub struct RenderGraph {
    nodes: Vec<Box<dyn RenderNode>>,
    connexions: HashMap<usize, HashMap<usize, Connexion>>,
    connexions_data: HashMap<usize, HashMap<String, Option<ConnexionData>>>,
    evaluation_order: BinaryHeap<Evaluation>,
}



impl RenderGraph {
    pub fn new (gl: &Gl) -> Self {
        let final_node = Box::new(ViewerNode::new(gl));

        let mut result = Self {
            nodes: Vec::new(),
            connexions: HashMap::new(),
            connexions_data: HashMap::new(),
            evaluation_order: BinaryHeap::new(),
        };
        result.add_node(final_node);

        return result;
    }

    pub fn final_node (&self) -> usize { 0 }


    pub fn add_node (&mut self, node: Box<dyn RenderNode>) -> usize {
        let output_names = node.get_output_names();
        self.nodes.push(node);

        let mut output_map : HashMap<String, Option<ConnexionData>> = HashMap::new();

        for name in output_names.iter() {
            output_map.insert(name.clone(), None);
        }

        self.connexions_data.insert(self.nodes.len() - 1, output_map);
        self.nodes.len() - 1
    }

    pub fn set_node_input (&mut self, node: usize, name: String, data: ConnexionData) {
        let node = Box::as_mut(self.nodes.get_mut(node).unwrap());

        node.set_input(name, Some(data));
    }

    pub fn connect_nodes (&mut self, from_node: usize, from_name: String, to_node: usize, to_name: String) {
        if self.connexions.contains_key(&to_node) == false {
            self.connexions.insert(to_node, HashMap::new());
        }

        match self.connexions.get_mut( &to_node ) {
            None => {},
            Some(map) => {
                map.insert(from_node, Connexion{
                    from_node,
                    from_name,
                    to_node,
                    to_name,
                });
            },
        }

        self.update_evaluation_order();
    }

    pub fn update (&mut self, gl: &Gl, delta_time: f32) {
        for node in self.nodes.iter_mut() {
            node.update(gl, delta_time);
        }
    }

    pub fn evaluate (&mut self, gl: &Gl) {
        let mut evaluation_order_copy = BinaryHeap::new();

        let mut visited = vec![false; self.nodes.len()];

        while self.evaluation_order.is_empty() == false {
            let current = self.evaluation_order.pop().unwrap();

            if visited[current.evaluation_target] == false {
                self.evaluate_node(gl, current.evaluation_target);

                visited[current.evaluation_target] = true;
            }

            evaluation_order_copy.push(current);
        }

        self.evaluation_order = evaluation_order_copy;
    }

    fn update_evaluation_order (&mut self) {
        self.evaluation_order.clear();

        let mut stack = Vec::new();
        let mut arrival = 0usize;

        stack.push(self.final_node());

        while stack.is_empty() == false {
            let current = stack.pop().unwrap();

            let connexions_opts = self.connexions.get(&current);

            arrival += 1;
            self.evaluation_order.push(Evaluation{
                evaluation_priority: arrival.clone(),
                evaluation_target: current.clone(),
            });

            match connexions_opts {
                None => {},
                Some(map) => {
                    for (from, _) in map.iter() {
                        stack.push(from.clone());
                    }
                },
            }
        }
    }

    fn evaluate_node (&mut self, gl: &Gl, node_id: usize) {
        if node_id >= self.nodes.len() {
            panic!("Invalid given node index: {}", node_id);
        }

        let node = Box::as_mut(self.nodes.get_mut(node_id).unwrap());

        // for each input in node, set the data inside from the connexion_datas vector
        let output_names = node.get_output_names();

        if output_names.len() == 0 && self.connexions.contains_key(&node_id) == false {
            return;
        }

        let default_map = HashMap::new();
        let from_map = self.connexions.get(&node_id).unwrap_or(&default_map);
        // The filter is normaly useless. But it seems better to be sure.
        for (from_node, connexion) in from_map.iter().filter(| (_a, b)| {
            b.to_node == node_id
        }) {
            let output_map = self.connexions_data.get(from_node).unwrap();
            node.set_input(connexion.to_name.clone(), output_map.get(&connexion.from_name).unwrap().clone());
        }

        node.evaluate(gl);

        for output_name in output_names.iter() {
            let output_map = self.connexions_data.get_mut(&node_id).unwrap();
            match node.get_output(output_name.clone()) {
                None => {},
                Some(output) => {
                    output_map.insert(output_name.clone(), output.data.clone());
                },
            }
        }
    }
}