use crate::gl::shader::shader_program::ShaderProgram;
use crate::graphical_objects::mesh_renderer::MeshRenderer;
use crate::render_graph::{RenderNode, NodeInterface};
use crate::gl::Gl;
use crate::render_graph::connexion::ConnexionData;
use crate::gl::gl_enum::*;
use crate::graphical_objects::camera::Camera;

use sp_scene::sp_mesh::sp_math::vector::Vector3f;


static VERT_CODE: &str = "#version 410 core

layout(location = 0) in vec3 in_pos;
layout(location = 1) in vec3 in_normal;
layout(location = 2) in vec2 in_tex_coord;

out VS_OUT {
    vec3 pos;
    vec3 normal;
    vec2 uv;
} vs_out;

void main() {
    vs_out.pos = in_pos;
    vs_out.normal = in_normal;
    vs_out.uv = in_tex_coord;

    gl_Position = vec4(in_pos, 1.0);
}";
static FRAG_CODE: &str = "#version 410 core

in VS_OUT {
    vec3 pos;
    vec3 normal;
    vec2 uv;
} fs_in;

uniform sampler2D ourTexture;

void main ()
{
    gl_FragColor = texture(ourTexture, fs_in.uv);
}
";

pub struct ViewerNode {
    input_render: NodeInterface,
    shader: ShaderProgram,
    quad: MeshRenderer,
}

impl ViewerNode {
    pub fn new (gl: &Gl) -> Self {

        let shader = ShaderProgram::from_code(
            gl,
            VERT_CODE.to_string(),
            FRAG_CODE.to_string(),
            None, None, None
        );
        shader.attach_shaders(&gl);
        shader.link(gl);

        Self {
            input_render: NodeInterface {
                name: "input_buffer".to_string(),
                data: None,
            },
            shader,
            quad : MeshRenderer::new(gl, &sp_scene::sp_mesh::generators::plane::make_view_plane()),
        }
    }
}

impl RenderNode for ViewerNode {
    fn evaluate(&mut self, gl: &Gl) {
        match &self.input_render.data {
            None => {},
            Some(data) => {
                gl.bind_frame_buffer(GL_FRAME_BUFFER, 0); // binding the default frame buffer (to be sure)
                gl.clear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
                gl.enable(GL_DEPTH_TEST);

                self.shader.use_program(gl);

                match data {
                    ConnexionData::Texture(input_tex) => {
                        input_tex.activate_bind(gl, GL_TEXTURE0, GL_TEXTURE_2D);
                    },
                    ConnexionData::RenderTarget(buf) => {
                        buf.texture().activate_bind(gl, GL_TEXTURE0, GL_TEXTURE_2D);
                    },
                    _ => {return}
                }

                self.quad.draw(
                    gl,
                    &Camera::new(Vector3f::from_one(1.0),Vector3f::from_one(0.0)),
                    &self.shader
                );
                self.shader.unuse_program(gl);
            },
        }
    }

    fn update(&mut self, _gl: &Gl, _delta_time: f32) {
        // nothing to do
    }

    fn get_input_names(&self) -> Vec<String> {
        vec!(self.input_render.name.clone())
    }

    fn set_input(&mut self, name: String, value: Option<ConnexionData>) {
        if name == self.input_render.name  && value.is_some() {
            self.input_render.data = value;
        }
    }

    fn get_output_names(&self) -> Vec<String> {
        Vec::new()
    }

    fn get_output(&self, _name: String) -> Option<&NodeInterface> {
        None
    }
}