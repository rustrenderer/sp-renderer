use crate::gl::Gl;
use crate::gl::gl_enum::*;

#[derive(Clone, Copy)]
pub struct Texture {
    pub id: GLuint,
    width: GLsizei,
    height: GLsizei,
    pub format: GLenum,
}

impl Texture {
    pub fn new (gl: &Gl, width: GLsizei, height: GLsizei, format: GLenum) -> Self {
        let mut ids: [GLuint; 1] = [0];

        gl.gen_textures(1, &mut ids);

        Self {
            id: ids[0],
            width,
            height,
            format,
        }
    }

    pub fn bind (&self, gl: &Gl, target: GLenum) {
        gl.bind_texture(target, self.id);
    }

    pub fn unbind (&self, gl: &Gl, target: GLenum) {
        gl.bind_texture(target, 0);
    }

    pub fn activate_bind (&self, gl: &Gl, unit: GLenum, target: GLenum) {
        gl.active_texture(unit);
        self.bind(gl, target);
    }

    pub fn set_data (&self, gl: &Gl, target: GLenum, data: Option<&[u8]>) {
        gl.tex_image_2d(
            target,
            0,
            self.format,
            self.width,
            self.height,
            0,
            self.format,
            GL_UNSIGNED_BYTE,
            data
        );
        gl.generate_mipmap(target);
    }
}