use crate::gl::Gl;
use crate::gl::gl_enum::{GLuint, GL_VERTEX_SHADER, GL_FRAGMENT_SHADER, GL_TESS_EVALUATION_SHADER, GL_TESS_CONTROL_SHADER, GL_GEOMETRY_SHADER};

use super::{Shader, Uniform};

#[derive(Clone)]
pub struct ShaderProgram {
    pub id: GLuint,
    vertex_shader: Option<Shader>,
    frag_shader: Option<Shader>,
    geom_shader: Option<Shader>,
    tess_control_shader: Option<Shader>,
    tess_eval_shader: Option<Shader>,
}

impl ShaderProgram {
    pub fn from_files (gl: &Gl, vert_file: String, frag_file: String) -> Self {
        let id: GLuint = gl.create_program();

        let vertex_shader = Shader::from_file(gl, GL_VERTEX_SHADER, vert_file);
        let frag_shader = Shader::from_file(gl, GL_FRAGMENT_SHADER, frag_file);

        Self {
            id,
            vertex_shader: Some(vertex_shader),
            frag_shader: Some(frag_shader),
            geom_shader: None,
            tess_control_shader: None,
            tess_eval_shader: None,
        }
    }

    pub fn from_code (gl: &Gl, vert_code: String, frag_code: String,
                      geom_code: Option<String>, tese_code: Option<String>, tesc_code: Option<String>)
        -> Self {
        let id: GLuint = gl.create_program();

        let vertex_shader = Shader::from_code(gl, GL_VERTEX_SHADER, vert_code);
        let frag_shader = Shader::from_code(gl, GL_FRAGMENT_SHADER, frag_code);

        let geo_shader = match  geom_code {
            Some(code) => Some(Shader::from_code(gl, GL_GEOMETRY_SHADER, code)),
            None => None
        };

        let tesc_shader = match  tesc_code {
            Some(code) => Some(Shader::from_code(gl, GL_GEOMETRY_SHADER, code)),
            None => None
        };

        let tese_shader = match  tese_code {
            Some(code) => Some(Shader::from_code(gl, GL_GEOMETRY_SHADER, code)),
            None => None
        };

        Self {
            id,
            vertex_shader: Some(vertex_shader),
            frag_shader: Some(frag_shader),
            geom_shader: geo_shader,
            tess_control_shader: tesc_shader,
            tess_eval_shader: tese_shader,
        }
    }

    pub fn add_tesselation_from_files (&mut self, gl: &Gl, eval_file: String, control_file: String) {
        let tess_control_shader = Shader::from_file(gl, GL_TESS_CONTROL_SHADER, control_file);
        let tess_eval_shader = Shader::from_file(gl, GL_TESS_EVALUATION_SHADER, eval_file);

        self.tess_control_shader = Some(tess_control_shader);
        self.tess_eval_shader = Some(tess_eval_shader);
    }


    pub fn add_geometry_from_files (&mut self, gl: &Gl, geometry_file: String) {
        let geometry_shader = Shader::from_file(gl, GL_GEOMETRY_SHADER, geometry_file);

        self.geom_shader = Some(geometry_shader);
    }

    pub fn attach_shaders (&self, gl: &Gl) {
        match &self.vertex_shader {
            Some(shader) => gl.attach_shader(self.id, shader.id),
            None => {}
        };
        match &self.tess_control_shader {
            Some(shader) => gl.attach_shader(self.id, shader.id),
            None => {}
        };
        match &self.tess_eval_shader {
            Some(shader) => gl.attach_shader(self.id, shader.id),
            None => {}
        };
        match &self.geom_shader {
            Some(shader) => gl.attach_shader(self.id, shader.id),
            None => {}
        };
        match &self.frag_shader {
            Some(shader) => gl.attach_shader(self.id, shader.id),
            None => {}
        };
    }

    pub fn detach_shaders (&self, gl: &Gl) {
        match &self.vertex_shader {
            Some(shader) => gl.detach_shader(self.id, shader.id),
            None => {}
        };
        match &self.tess_control_shader {
            Some(shader) => gl.detach_shader(self.id, shader.id),
            None => {}
        };
        match &self.tess_eval_shader {
            Some(shader) => gl.detach_shader(self.id, shader.id),
            None => {}
        };
        match &self.geom_shader {
            Some(shader) => gl.detach_shader(self.id, shader.id),
            None => {}
        };
        match &self.frag_shader {
            Some(shader) => gl.detach_shader(self.id, shader.id),
            None => {}
        };
    }

    pub fn clean (&self, gl: &Gl) {
        gl.use_program(0);
        self.detach_shaders(gl);

        match &self.vertex_shader {
            Some(shader) => gl.delete_shader(shader.id),
            None => {}
        };
        match &self.frag_shader {
            Some(shader) => gl.delete_shader(shader.id),
            None => {}
        };
        match &self.geom_shader {
            Some(shader) => gl.delete_shader(shader.id),
            None => {}
        };
        match &self.tess_control_shader {
            Some(shader) => gl.delete_shader(shader.id),
            None => {}
        };
        match &self.tess_eval_shader {
            Some(shader) => gl.delete_shader(shader.id),
            None => {}
        };

        gl.delete_program(self.id);
    }

    pub fn reload (&mut self, gl: &Gl) {
        self.clean(gl);
        self.id = gl.create_program();

        match &self.vertex_shader {
            Some(shader) => self.vertex_shader = Some(shader.reload(gl)),
            None => {}
        };
        match &self.frag_shader {
            Some(shader) => self.frag_shader = Some(shader.reload(gl)),
            None => {}
        };
        match &self.geom_shader {
            Some(shader) => self.geom_shader = Some(shader.reload(gl)),
            None => {}
        };
        match &self.tess_control_shader {
            Some(shader) => self.tess_control_shader = Some(shader.reload(gl)),
            None => {}
        };
        match &self.tess_eval_shader {
            Some(shader) => self.tess_eval_shader = Some(shader.reload(gl)),
            None => {}
        };
    }

    pub fn link (&self, gl: &Gl) {
        gl.link_program(self.id);
    }

    pub fn use_program (&self, gl: &Gl) {
        gl.use_program(self.id);
    }
    pub fn unuse_program (&self, gl: &Gl) {
        gl.use_program(0);
    }

    pub fn get_attrib_location (&self, gl: &Gl, attrib_name: String) -> GLuint {
        gl.get_attrib_location(self.id, attrib_name)
    }

    pub fn get_uniform_location (&self, gl: &Gl, name: String) -> Uniform {
        Uniform {
            id: gl.get_uniform_location(self.id, name),
            shader_id: self.id,
        }
    }

    pub fn has_tesselation (&self) -> bool {
        match &self.tess_control_shader {
            Some(_) => true,
            None => false
        }
    }
}