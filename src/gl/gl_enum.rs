use super::_gl;

pub type GLbool = _gl::types::GLboolean;

pub type GLbyte = _gl::types::GLbyte;
pub type GLubyte = _gl::types::GLubyte;

pub type GLchar = _gl::types::GLchar;

pub type GLshort = _gl::types::GLshort;
pub type GLushort = _gl::types::GLushort;

pub type GLint = _gl::types::GLint;
pub type GLuint = _gl::types::GLuint;

pub type GLsizei = _gl::types::GLsizei;
pub type GLsizeiptr = _gl::types::GLsizeiptr;

pub type GLint64 = _gl::types::GLint64;
pub type GLuint64 = _gl::types::GLuint64;

pub type GLhalf = _gl::types::GLhalf;
pub type GLfloat = _gl::types::GLfloat;
pub type GLdouble = _gl::types::GLdouble;

pub type GLenum = _gl::types::GLenum;

pub type GLbitfield = _gl::types::GLbitfield;

/* ClearBufferMask */
pub const GL_DEPTH_BUFFER_BIT : GLenum = _gl::DEPTH_BUFFER_BIT;
pub const GL_STENCIL_BUFFER_BIT : GLenum = _gl::STENCIL_BUFFER_BIT;
pub const GL_COLOR_BUFFER_BIT : GLenum = _gl::COLOR_BUFFER_BIT;

/* Boolean */
pub const GL_FALSE : GLbool = _gl::FALSE;
pub const GL_TRUE : GLbool = _gl::TRUE;

/* BeginMode */
pub const GL_POINTS : GLenum = _gl::POINTS;
pub const GL_LINES : GLenum = _gl::LINES;
pub const GL_LINE_LOOP : GLenum = _gl::LINE_LOOP;
pub const GL_LINE_STRIP : GLenum = _gl::LINE_STRIP;
pub const GL_TRIANGLES : GLenum = _gl::TRIANGLES;
pub const GL_TRIANGLE_STRIP : GLenum = _gl::TRIANGLE_STRIP;
pub const GL_TRIANGLE_FAN : GLenum = _gl::TRIANGLE_FAN;
pub const GL_PATCHES : GLenum = _gl::PATCHES;

/* AlphaFunction */
pub const GL_LEQUAL : GLenum = _gl::LEQUAL;
pub const GL_ALWAYS : GLenum = _gl::ALWAYS;

/* BlendingFactorDest */
pub const GL_ZERO : GLenum = _gl::ZERO;
/*    pub const GL_ONE : GlEnum = _gl::ONE;*/
pub const GL_ONE_MINUS_SRC_ALPHA : GLenum = _gl::ONE_MINUS_SRC_ALPHA;

/* BlendingFactorSrc */
pub const GL_ONE : GLenum = _gl::ONE;
pub const GL_SRC_ALPHA_SATURATE : GLenum = _gl::SRC_ALPHA_SATURATE;
pub const GL_SRC_ALPHA : GLenum = _gl::SRC_ALPHA;
pub const GL_DST_ALPHA : GLenum = _gl::DST_ALPHA;
pub const GL_SRC_COLOR : GLenum = _gl::SRC_COLOR;
pub const GL_DST_COLOR : GLenum = _gl::DST_COLOR;

/* ColorPointerType */
pub const GL_FLOAT : GLenum = _gl::FLOAT;
pub const GL_UNSIGNED_BYTE : GLenum = _gl::UNSIGNED_BYTE;

/* CullFaceMode */
pub const GL_FRONT : GLenum = _gl::FRONT;
pub const GL_BACK : GLenum = _gl::BACK;
pub const GL_FRONT_AND_BACK : GLenum = _gl::FRONT_AND_BACK;

/* DepthFunction */
/*  pub const GL_LESS : GlEnum = _gl::LESS; */
/*  pub const GL_LEQUAL : GlEnum = _gl::LEQUAL; */
/*  pub const GL_ALWAYS : GlEnum = _gl::ALWAYS; */

/* EnableCap */
pub const GL_TEXTURE_2D : GLenum = _gl::TEXTURE_2D;
pub const GL_CULL_FACE : GLenum = _gl::CULL_FACE;
pub const GL_BLEND : GLenum = _gl::BLEND;
pub const GL_STENCIL_TEST : GLenum = _gl::STENCIL_TEST;
pub const GL_DEPTH_TEST : GLenum = _gl::DEPTH_TEST;
pub const GL_LINE_SMOOTH : GLenum = _gl::LINE_SMOOTH;
pub const GL_SCISSOR_TEST : GLenum = _gl::SCISSOR_TEST;
pub const GL_POLYGON_OFFSET_FILL : GLenum = _gl::POLYGON_OFFSET_FILL;

/* ErrorCode */
pub const GL_NO_ERROR : GLenum = _gl::NO_ERROR;
pub const GL_INVALID_ENUM : GLenum = _gl::INVALID_ENUM;
pub const GL_INVALID_VALUE : GLenum = _gl::INVALID_VALUE;
pub const GL_INVALID_OPERATION : GLenum = _gl::INVALID_OPERATION;
pub const GL_OUT_OF_MEMORY : GLenum = _gl::OUT_OF_MEMORY;
pub const GL_STACK_OVERFLOW : GLenum = _gl::STACK_OVERFLOW;
pub const GL_STACK_UNDERFLOW : GLenum = _gl::STACK_UNDERFLOW;
pub const GL_INVALID_FRAMEBUFFER_OPERATION : GLenum = _gl::INVALID_FRAMEBUFFER_OPERATION;

/* FrontFaceDirection */
pub const GL_CW : GLenum = _gl::CW;
pub const GL_CCW : GLenum = _gl::CCW;

/* GetBooleanv */
pub const GL_DEPTH_WRITEMASK : GLenum = _gl::DEPTH_WRITEMASK;
pub const GL_COLOR_WRITEMASK : GLenum = _gl::COLOR_WRITEMASK;

/* GetFloatv */
pub const GL_POINT_SIZE : GLenum = _gl::POINT_SIZE;
pub const GL_SMOOTH_POINT_SIZE_RANGE : GLenum = _gl::SMOOTH_POINT_SIZE_RANGE;
pub const GL_SMOOTH_POINT_SIZE_GRANULARITY : GLenum = _gl::SMOOTH_POINT_SIZE_GRANULARITY;
pub const GL_LINE_WIDTH : GLenum = _gl::LINE_WIDTH;
pub const GL_SMOOTH_LINE_WIDTH_RANGE : GLenum = _gl::SMOOTH_LINE_WIDTH_RANGE;
pub const GL_SMOOTH_LINE_WIDTH_GRANULARITY : GLenum = _gl::SMOOTH_LINE_WIDTH_GRANULARITY;
pub const GL_DEPTH_RANGE : GLenum = _gl::DEPTH_RANGE;
pub const GL_DEPTH_CLEAR_VALUE : GLenum = _gl::DEPTH_CLEAR_VALUE;
pub const GL_COLOR_CLEAR_VALUE : GLenum = _gl::COLOR_CLEAR_VALUE;
pub const GL_POLYGON_OFFSET_UNITS : GLenum = _gl::POLYGON_OFFSET_UNITS;
pub const GL_POLYGON_OFFSET_FACTOR : GLenum = _gl::POLYGON_OFFSET_FACTOR;
pub const GL_ALIASED_LINE_WIDTH_RANGE : GLenum = _gl::ALIASED_LINE_WIDTH_RANGE;

/* GetIntegerv */
pub const GL_VIEWPORT : GLenum = _gl::VIEWPORT;
pub const GL_CULL_FACE_MODE : GLenum = _gl::CULL_FACE_MODE;
pub const GL_FRONT_FACE : GLenum = _gl::FRONT_FACE;
pub const GL_DEPTH_FUNC : GLenum = _gl::DEPTH_FUNC;
pub const GL_STENCIL_CLEAR_VALUE : GLenum = _gl::STENCIL_CLEAR_VALUE;
pub const GL_STENCIL_FUNC : GLenum = _gl::STENCIL_FUNC;
pub const GL_STENCIL_VALUE_MASK : GLenum = _gl::STENCIL_VALUE_MASK;
pub const GL_STENCIL_FAIL : GLenum = _gl::STENCIL_FAIL;
pub const GL_STENCIL_PASS_DEPTH_FAIL : GLenum = _gl::STENCIL_PASS_DEPTH_FAIL;
pub const GL_STENCIL_PASS_DEPTH_PASS : GLenum = _gl::STENCIL_PASS_DEPTH_PASS;
pub const GL_STENCIL_REF : GLenum = _gl::STENCIL_REF;
pub const GL_STENCIL_WRITEMASK : GLenum = _gl::STENCIL_WRITEMASK;
pub const GL_BLEND_DST : GLenum = _gl::BLEND_DST;
pub const GL_BLEND_SRC : GLenum = _gl::BLEND_SRC;
pub const GL_SCISSOR_BOX : GLenum = _gl::SCISSOR_BOX;
pub const GL_LINE_SMOOTH_HINT : GLenum = _gl::LINE_SMOOTH_HINT;
pub const GL_POLYGON_SMOOTH_HINT : GLenum = _gl::POLYGON_SMOOTH_HINT;
pub const GL_UNPACK_ALIGNMENT : GLenum = _gl::UNPACK_ALIGNMENT;
pub const GL_PACK_ALIGNMENT : GLenum = _gl::PACK_ALIGNMENT;
pub const GL_MAX_TEXTURE_SIZE : GLenum = _gl::MAX_TEXTURE_SIZE;
pub const GL_MAX_VIEWPORT_DIMS : GLenum = _gl::MAX_VIEWPORT_DIMS;
pub const GL_SUBPIXEL_BITS : GLenum = _gl::SUBPIXEL_BITS;
pub const GL_TEXTURE_BINDING_2D : GLenum = _gl::TEXTURE_BINDING_2D;
pub const GL_MAX_ELEMENTS_VERTICES : GLenum = _gl::MAX_ELEMENTS_VERTICES;
pub const GL_MAX_ELEMENTS_INDICES : GLenum = _gl::MAX_ELEMENTS_INDICES;
pub const GL_ACTIVE_TEXTURE : GLenum = _gl::ACTIVE_TEXTURE;

/* GetTexParameter */
/*  pub const GL_TEXTURE_MAG_FILTER : GlEnum = _gl::TEXTURE_MAG_FILTER; */
/*  pub const GL_TEXTURE_MIN_FILTER : GlEnum = _gl::TEXTURE_MIN_FILTER; */
/*  pub const GL_TEXTURE_WRAP_S : GlEnum = _gl::TEXTURE_WRAP_S; */
/*  pub const GL_TEXTURE_WRAP_T : GlEnum = _gl::TEXTURE_WRAP_T; */

/* HintMode */
pub const GL_DONT_CARE : GLenum = _gl::DONT_CARE;
pub const GL_FASTEST : GLenum = _gl::FASTEST;
pub const GL_NICEST : GLenum = _gl::NICEST;

/* DataType */
pub const GL_BYTE : GLenum = _gl::BYTE;
pub const GL_UNSIGNED_INT : GLenum = _gl::UNSIGNED_INT;
/* pub const GL_UNSIGNED_BYTE : GlEnum = _gl::UNSIGNED_BYTE; */
/* pub const GL_FLOAT : GlEnum = _gl::FLOAT; */

/* MaterialFace */
/*  pub const GL_FRONT_AND_BACK : GlEnum = _gl::FRONT_AND_BACK; */

/* NormalPointerType */
/*  pub const GL_FLOAT : GlEnum = _gl::FLOAT; */

/* PixelFormat */
pub const GL_ALPHA : GLenum = _gl::ALPHA;
pub const GL_RGB : GLenum = _gl::RGB;
pub const GL_RGBA : GLenum = _gl::RGBA;

/* PixelType */
/*  pub const GL_UNSIGNED_BYTE : GlEnum = _gl::UNSIGNED_BYTE; */

/* ReadPixels */
pub const GL_COLOR : GLenum = _gl::COLOR;

/* StencilFunction */
pub const GL_NEVER : GLenum = _gl::NEVER;
pub const GL_LESS : GLenum = _gl::LESS;
pub const GL_EQUAL : GLenum = _gl::EQUAL;
/* pub const GL_LEQUAL : GlEnum = _gl::LEQUAL; */
pub const GL_GREATER : GLenum = _gl::GREATER;
pub const GL_NOTEQUAL : GLenum = _gl::NOTEQUAL;
pub const GL_GEQUAL : GLenum = _gl::GEQUAL;
/* pub const GL_ALWAYS : GlEnum = _gl::ALWAYS; */

/* StencilOp */
/*  pub const GL_ZERO : GlEnum = _gl::ZERO; */
pub const GL_KEEP : GLenum = _gl::KEEP;
pub const GL_REPLACE : GLenum = _gl::REPLACE;
pub const GL_INCR : GLenum = _gl::INCR;
pub const GL_DECR : GLenum = _gl::DECR;
pub const GL_INVERT : GLenum = _gl::INVERT;

/* StringName */
pub const GL_VENDOR : GLenum = _gl::VENDOR;
pub const GL_RENDERER : GLenum = _gl::RENDERER;
pub const GL_VERSION : GLenum = _gl::VERSION;
pub const GL_EXTENSIONS : GLenum = _gl::EXTENSIONS;

/* TexCoordPointerType */
/*  pub const GL_FLOAT : GlEnum = _gl::FLOAT; */

/* TextureMagFilter */
pub const GL_NEAREST : GLenum = _gl::NEAREST;
pub const GL_LINEAR : GLenum = _gl::LINEAR;

/* TextureMinFilter */
/*  pub const GL_NEAREST : GlEnum = _gl::NEAREST; */
/*  pub const GL_LINEAR : GlEnum = _gl::LINEAR; */
pub const GL_NEAREST_MIPMAP_NEAREST : GLenum = _gl::NEAREST_MIPMAP_NEAREST;
pub const GL_LINEAR_MIPMAP_NEAREST : GLenum = _gl::LINEAR_MIPMAP_NEAREST;
pub const GL_NEAREST_MIPMAP_LINEAR : GLenum = _gl::NEAREST_MIPMAP_LINEAR;
pub const GL_LINEAR_MIPMAP_LINEAR : GLenum = _gl::LINEAR_MIPMAP_LINEAR;

/* TextureParameterName */
pub const GL_TEXTURE_MAG_FILTER : GLenum = _gl::TEXTURE_MAG_FILTER;
pub const GL_TEXTURE_MIN_FILTER : GLenum = _gl::TEXTURE_MIN_FILTER;
pub const GL_TEXTURE_WRAP_S : GLenum = _gl::TEXTURE_WRAP_S;
pub const GL_TEXTURE_WRAP_T : GLenum = _gl::TEXTURE_WRAP_T;

/* TextureTarget */
/*  pub const GL_TEXTURE_2D : GlEnum = _gl::TEXTURE_2D; */

/* TextureUnit */
pub const GL_TEXTURE0 : GLenum = _gl::TEXTURE0;
pub const GL_TEXTURE1 : GLenum = _gl::TEXTURE1;
pub const GL_TEXTURE2 : GLenum = _gl::TEXTURE2;
pub const GL_TEXTURE3 : GLenum = _gl::TEXTURE3;
pub const GL_TEXTURE4 : GLenum = _gl::TEXTURE4;
pub const GL_TEXTURE5 : GLenum = _gl::TEXTURE5;
pub const GL_TEXTURE6 : GLenum = _gl::TEXTURE6;
pub const GL_TEXTURE7 : GLenum = _gl::TEXTURE7;
pub const GL_TEXTURE8 : GLenum = _gl::TEXTURE8;
pub const GL_TEXTURE9 : GLenum = _gl::TEXTURE9;
pub const GL_TEXTURE10 : GLenum = _gl::TEXTURE10;
pub const GL_TEXTURE11 : GLenum = _gl::TEXTURE11;
pub const GL_TEXTURE12 : GLenum = _gl::TEXTURE12;
pub const GL_TEXTURE13 : GLenum = _gl::TEXTURE13;
pub const GL_TEXTURE14 : GLenum = _gl::TEXTURE14;
pub const GL_TEXTURE15 : GLenum = _gl::TEXTURE15;
pub const GL_TEXTURE16 : GLenum = _gl::TEXTURE16;
pub const GL_TEXTURE17 : GLenum = _gl::TEXTURE17;
pub const GL_TEXTURE18 : GLenum = _gl::TEXTURE18;
pub const GL_TEXTURE19 : GLenum = _gl::TEXTURE19;
pub const GL_TEXTURE20 : GLenum = _gl::TEXTURE20;
pub const GL_TEXTURE21 : GLenum = _gl::TEXTURE21;
pub const GL_TEXTURE22 : GLenum = _gl::TEXTURE22;
pub const GL_TEXTURE23 : GLenum = _gl::TEXTURE23;
pub const GL_TEXTURE24 : GLenum = _gl::TEXTURE24;
pub const GL_TEXTURE25 : GLenum = _gl::TEXTURE25;
pub const GL_TEXTURE26 : GLenum = _gl::TEXTURE26;
pub const GL_TEXTURE27 : GLenum = _gl::TEXTURE27;
pub const GL_TEXTURE28 : GLenum = _gl::TEXTURE28;
pub const GL_TEXTURE29 : GLenum = _gl::TEXTURE29;
pub const GL_TEXTURE30 : GLenum = _gl::TEXTURE30;
pub const GL_TEXTURE31 : GLenum = _gl::TEXTURE31;

/* TextureWrapMode */
pub const GL_REPEAT : GLenum = _gl::REPEAT;
pub const GL_CLAMP_TO_EDGE : GLenum = _gl::CLAMP_TO_EDGE;

pub const GL_ARRAY_BUFFER : GLenum = _gl::ARRAY_BUFFER;
pub const GL_ELEMENT_ARRAY_BUFFER : GLenum = _gl::ELEMENT_ARRAY_BUFFER;
pub const GL_SHADER_STORAGE_BUFFER : GLenum = _gl::SHADER_STORAGE_BUFFER;
pub const GL_FRAME_BUFFER : GLenum = _gl::FRAMEBUFFER;

pub const GL_RENDER_BUFFER : GLenum = _gl::RENDERBUFFER;

pub const GL_STATIC_DRAW : GLenum = _gl::STATIC_DRAW;

/* VertexPointerType */
/*  pub const GL_FLOAT : GlEnum = _gl::FLOAT; */

/* Shader types */
pub const GL_VERTEX_SHADER : GLenum = _gl::VERTEX_SHADER;
pub const GL_FRAGMENT_SHADER : GLenum = _gl::FRAGMENT_SHADER;
pub const GL_GEOMETRY_SHADER : GLenum = _gl::GEOMETRY_SHADER;
pub const GL_TESS_EVALUATION_SHADER : GLenum = _gl::TESS_EVALUATION_SHADER;
pub const GL_TESS_CONTROL_SHADER : GLenum = _gl::TESS_CONTROL_SHADER;
pub const GL_COMPUTE_SHADER : GLenum = _gl::COMPUTE_SHADER;

pub const GL_FUNC_ADD : GLenum = _gl::FUNC_ADD;

pub const GL_COLOR_ATTACHMENT0 : GLenum = _gl::COLOR_ATTACHMENT0;
pub const GL_COLOR_ATTACHMENT1 : GLenum = _gl::COLOR_ATTACHMENT1;
pub const GL_COLOR_ATTACHMENT2 : GLenum = _gl::COLOR_ATTACHMENT2;
pub const GL_COLOR_ATTACHMENT3 : GLenum = _gl::COLOR_ATTACHMENT3;
pub const GL_COLOR_ATTACHMENT4 : GLenum = _gl::COLOR_ATTACHMENT4;
pub const GL_COLOR_ATTACHMENT5 : GLenum = _gl::COLOR_ATTACHMENT5;
pub const GL_COLOR_ATTACHMENT6 : GLenum = _gl::COLOR_ATTACHMENT6;
pub const GL_COLOR_ATTACHMENT7 : GLenum = _gl::COLOR_ATTACHMENT7;
pub const GL_COLOR_ATTACHMENT8 : GLenum = _gl::COLOR_ATTACHMENT8;
pub const GL_COLOR_ATTACHMENT9 : GLenum = _gl::COLOR_ATTACHMENT9;
pub const GL_COLOR_ATTACHMENT10 : GLenum = _gl::COLOR_ATTACHMENT10;
pub const GL_COLOR_ATTACHMENT11 : GLenum = _gl::COLOR_ATTACHMENT11;
pub const GL_COLOR_ATTACHMENT12 : GLenum = _gl::COLOR_ATTACHMENT12;
pub const GL_COLOR_ATTACHMENT13 : GLenum = _gl::COLOR_ATTACHMENT13;
pub const GL_COLOR_ATTACHMENT14 : GLenum = _gl::COLOR_ATTACHMENT14;
pub const GL_COLOR_ATTACHMENT15 : GLenum = _gl::COLOR_ATTACHMENT15;
pub const GL_COLOR_ATTACHMENT16 : GLenum = _gl::COLOR_ATTACHMENT16;
pub const GL_COLOR_ATTACHMENT17 : GLenum = _gl::COLOR_ATTACHMENT17;
pub const GL_COLOR_ATTACHMENT18 : GLenum = _gl::COLOR_ATTACHMENT18;
pub const GL_COLOR_ATTACHMENT19 : GLenum = _gl::COLOR_ATTACHMENT19;
pub const GL_COLOR_ATTACHMENT20 : GLenum = _gl::COLOR_ATTACHMENT20;
pub const GL_COLOR_ATTACHMENT21 : GLenum = _gl::COLOR_ATTACHMENT21;
pub const GL_COLOR_ATTACHMENT22 : GLenum = _gl::COLOR_ATTACHMENT22;
pub const GL_COLOR_ATTACHMENT23 : GLenum = _gl::COLOR_ATTACHMENT23;
pub const GL_COLOR_ATTACHMENT24 : GLenum = _gl::COLOR_ATTACHMENT24;
pub const GL_COLOR_ATTACHMENT25 : GLenum = _gl::COLOR_ATTACHMENT25;
pub const GL_COLOR_ATTACHMENT26 : GLenum = _gl::COLOR_ATTACHMENT26;
pub const GL_COLOR_ATTACHMENT27 : GLenum = _gl::COLOR_ATTACHMENT27;
pub const GL_COLOR_ATTACHMENT28 : GLenum = _gl::COLOR_ATTACHMENT28;
pub const GL_COLOR_ATTACHMENT29 : GLenum = _gl::COLOR_ATTACHMENT29;
pub const GL_COLOR_ATTACHMENT30 : GLenum = _gl::COLOR_ATTACHMENT30;
pub const GL_COLOR_ATTACHMENT31 : GLenum = _gl::COLOR_ATTACHMENT31;

pub const GL_DEPTH24_STENCIL8 : GLenum = _gl::DEPTH24_STENCIL8;

pub const GL_DEPTH_STENCIL_ATTACHMENT : GLenum = _gl::DEPTH_STENCIL_ATTACHMENT;