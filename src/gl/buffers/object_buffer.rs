use crate::gl::gl_enum::*;
use crate::gl::Gl;
use std::marker::PhantomData;


pub trait BufferTarget {
    fn target () -> GLenum;
}

#[derive(Clone)]
pub struct ObjectBuffer<T>
    where T: BufferTarget
{
    pub id: GLuint,
    pub target: GLenum,
    phantom: PhantomData<T>,
}

impl<T> ObjectBuffer<T>
where T: BufferTarget
{
    pub fn new (gl: &Gl) -> Self {
        let mut ids : [GLuint;1] = [0];
        gl.gen_buffers(1, &mut ids);

        Self {
            id: ids[0],
            target: T::target(),
            phantom: PhantomData,
        }
    }

    pub fn bind (&self, gl: &Gl) {
        gl.bind_buffer(self.target, self.id);
    }

    pub fn set_data<D> (&self, gl: &Gl, data: &[D], usage: GLenum) {
        gl.buffer_data(self.target, data, usage);
    }

    pub fn delete (&self, gl: &Gl) {
        gl.delete_buffers(1, &[self.id]);
    }
}