mod object_buffer;

use object_buffer::{BufferTarget, ObjectBuffer};
use crate::gl::gl_enum::*;
use crate::gl::Gl;
use crate::gl::texture::Texture;


mod targets{
    #[derive(Clone)]
    pub struct IBOTarget {}
    impl super::BufferTarget for IBOTarget {
        fn target() -> super::GLenum {
            super::GL_ELEMENT_ARRAY_BUFFER
        }
    }

    #[derive(Clone)]
    pub struct VBOTarget {}
    impl super::BufferTarget for VBOTarget {
        fn target() -> super::GLenum {
            super::GL_ARRAY_BUFFER
        }
    }

    #[derive(Clone)]
    pub struct SSBOTarget {}
    impl super::BufferTarget for SSBOTarget {
        fn target() -> super::GLenum {
            super::GL_ELEMENT_ARRAY_BUFFER
        }
    }
}

pub type IBO = ObjectBuffer<targets::IBOTarget>;
pub type VBO = ObjectBuffer<targets::VBOTarget>;
pub type SSBO = ObjectBuffer<targets::SSBOTarget>;

#[derive(Clone, Copy)]
pub struct VAO {
    pub id: GLuint,
}

impl VAO {
    pub fn new (gl: &Gl) -> Self {
        let mut ids: [GLuint;1] = [0];
        gl.gen_vertex_arrays(1, &mut ids);

        Self {
            id: ids[0],
        }
    }

    pub fn bind (&self, gl: &Gl) {
        gl.bind_vertex_array(self.id);
    }
    pub fn unbind (&self, gl: &Gl) { gl.bind_vertex_array(0); }

    pub fn clean (&self, _gl: &Gl) {
    }
}

#[derive(Clone, Copy)]
pub struct FBO {
    pub id: GLuint,
}

impl FBO {
    pub fn new (gl: &Gl) -> Self {
        let mut ids: [GLuint;1] = [0];
        gl.gen_frame_buffers(1, &mut ids);

        Self {
            id: ids[0],
        }
    }

    pub fn bind (&self, gl: &Gl) {
        gl.bind_frame_buffer(GL_FRAME_BUFFER, self.id);
    }

    pub fn unbind (&self, gl: &Gl) {
        gl.bind_frame_buffer(GL_FRAME_BUFFER, 0);
    }

    pub fn clean (&self, gl: &Gl) {
        gl.delete_frame_buffers(1, &[self.id]);
    }

    pub fn add_texture_attachment (&self, gl: &Gl, attachment: GLenum, texture: &Texture, level: i32) {
        gl.frame_buffer_texture_2d(GL_FRAME_BUFFER, attachment, GL_TEXTURE_2D, texture.id, level);
    }

    pub fn add_rbo_attachment (&self, gl: &Gl, attachment: GLenum, rbo: &RBO) {
        gl.frame_buffer_render_buffer(GL_FRAME_BUFFER, attachment, GL_RENDER_BUFFER, rbo.id);
    }
}

#[derive(Clone, Copy)]
pub struct RBO {
    pub id: GLuint,
}

impl RBO {
    pub fn new (gl: &Gl) -> Self {
        let mut ids: [GLuint;1] = [0];
        gl.gen_render_buffers(1, &mut ids);

        Self {
            id: ids[0],
        }
    }

    pub fn bind (&self, gl: &Gl) {
        gl.bind_render_buffer(GL_RENDER_BUFFER, self.id);
    }

    pub fn unbind (&self, gl: &Gl) {
        gl.bind_render_buffer(GL_RENDER_BUFFER, 0);
    }

    pub fn set_storage (&self, gl: &Gl, internal_format: GLenum, width: GLsizei, height: GLsizei) {
        gl.render_buffer_storage(GL_RENDER_BUFFER, internal_format, width, height);
    }

    pub fn clean (&self, gl: &Gl) {
        gl.delete_render_buffers(1, &[self.id]);
    }

}

